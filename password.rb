#!/usr/bin/env ruby

words = %w(about every large plant spell these where after first learn point stil thing which again found never right study think world below great other small their three would could house place sound there water write)

puts "Welcome to the Keep Talking Password Cracker"
puts "############################################"
puts ""

def match(words, letters, position)
  words.select { |word| letters.include? word[position] }
end

def get_letters(words, i)
  if words.map { |word| word[i] }.uniq.length == 1
    puts "Position #{i+1} must be '#{words[0][i]}', no need to enter anything"
    letters = words[0][i]
  else
    puts "Enter letters for position #{i+1}"
    letters = gets.chomp
  end
  letters
end

def remaining_words(words)
  puts
  puts "Remaining Words"
  puts "###############"
  puts
  puts words
  puts
end

0.upto(4) do |i|
  letters = get_letters(words, i)
  words = match(words, letters, i)
  remaining_words(words)
end
